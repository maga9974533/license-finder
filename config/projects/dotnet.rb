# frozen_string_literal: true

dotnet_version = ENV.fetch('DOTNET_VERSION', '3.1.302')

name "dotnet-#{dotnet_version}"
maintainer "GitLab B.V."
homepage "https://docs.microsoft.com/en-us/dotnet/core/"

install_dir "/opt/asdf/installs/dotnet-core/#{dotnet_version}"
package_scripts_path Pathname.pwd.join("config/scripts/dotnet")

build_version dotnet_version
build_iteration 1

dependency "asdf_dotnet_sdk"

package :deb do
  compression_level 9
  compression_type :xz
end
