# frozen_string_literal: true

rust_version = ENV.fetch('RUST_VERSION', '1.45.0')

name "rust-#{rust_version}"
maintainer "GitLab B.V."
homepage "https://github.com/rust-lang/rust"

install_dir "/opt/asdf/installs/rust/#{rust_version}"
package_scripts_path Pathname.pwd.join("config/scripts/rust")

build_version rust_version
build_iteration 1

override 'asdf_rust', version: rust_version
dependency "asdf_rust"

package :deb do
  compression_level 9
  compression_type :xz
end
