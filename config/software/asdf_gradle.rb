# frozen_string_literal: true

name "asdf_gradle"
default_version "6.9.2"

source url: "https://services.gradle.org/distributions/gradle-#{version}-bin.zip"
relative_path "gradle-#{version}"

version "6.9.2" do
  source sha256: "8b356fd8702d5ffa2e066ed0be45a023a779bba4dd1a68fd11bc2a6bdc981e8f"
end

build do
  mkdir install_dir
  copy "#{project_dir}/**", "#{install_dir}/"
end
