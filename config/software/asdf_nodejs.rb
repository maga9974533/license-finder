# frozen_string_literal: true

name "asdf_nodejs"
default_version "14.17.1"

source url: "https://nodejs.org/dist/v#{version}/node-v#{version}-linux-x64.tar.gz"
relative_path "node-v#{version}-linux-x64"

version "14.17.1" do
  # See https://nodejs.org/dist/v14.17.1/SHASUMS256.txt
  source sha256: "4781b162129b19bdb3a7010cab12d06fc7c89421ea3fda03346ed17f09ceacd6"
end

version "12.18.2" do
  # See https://nodejs.org/dist/v12.18.2/SHASUMS256.txt
  source sha256: "2d316e55994086e41761b0c657e0027e9d16d7160d3f8854cc9dc7615b99a526"
end

version "10.21.0" do
  # See https://nodejs.org/dist/v10.21.0/SHASUMS256.txt
  source sha256: "d0bac246001eed9268ba9cadbfc6cfd8b6eb0728ad000a0f9fa7ce29e66c2be4"
end

build do
  mkdir install_dir
  sync "#{project_dir}/", install_dir
  touch "#{install_dir}/.npm/.keep"
end

build do
  command "#{install_dir}/bin/node #{install_dir}/bin/npm install -g bower bower-npm-resolver npm@6 npm-install-peers yarn"
end

build do
  delete "#{install_dir}/share"
end
