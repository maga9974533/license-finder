# frozen_string_literal: true

module LicenseFinder
  class Gradle
    def prepare
      within_project_path do
        tool_box.install(tool: :java, version: java_version, env: default_env)
      end
    end

    def current_packages
      return [] unless download_licenses

      Pathname
        .glob(project_path.join('**', 'dependency-license.xml'))
        .map(&:read)
        .flat_map { |xml_file| parse_from(xml_file) }.uniq
    end

    def package_management_command
      wrapper? ? project_path.join('gradlew') : :gradle
    end

    private

    def java_version(env: ENV)
      @java_version ||= tool_box.version_of(:java, env: env)
    end

    def gradle_version(env: ENV)
      stdout, _stderr, _status = within_project_path do
        shell.execute([
          @command,
          '--version'
        ], env: env)
      end

      # The gradle output looks like the following:
      #
      # ------------------------------------------------------------
      # Gradle 6.5.1
      # ------------------------------------------------------------
      #
      # Build time:   2020-06-30 06:32:47 UTC
      # Revision:     66bc713f7169626a7f0134bf452abde51550ea0a
      #
      # Kotlin:       1.3.72
      # Groovy:       2.5.11
      # Ant:          Apache Ant(TM) version 1.10.7 compiled on September 1 2019
      # JVM:          1.8.0_262 (AdoptOpenJDK 25.262-b10)
      # OS:           Linux 5.15.40-0-virt amd64
      #
      # This regex extraction will take the version string from the output.
      matches = stdout.match(/Gradle (\S+)/)
      matches.length > 1 ? matches[1] : ""
    end

    def download_licenses
      gradle_cli_opts = ENV.fetch('GRADLE_CLI_OPTS', '--exclude-task=test --no-daemon --debug')
      # There are two versions of the init script due to Gradle compatibility issues.
      # The latest version of the LF plugin that works with Gradle 2.X - 6.X is 0.15.0.
      # Gradle 7 projects will only work with version 0.16.1 of the plugin. Therfore,
      # there are two scripts "legacy" and "current" which utilize versions 0.15.0 and
      # 0.16.1 respectively. At runtime, the Gradle version will determine which init
      # script is utilized.
      gradle_init_script_version = gradle_version < '7' ? 'legacy-init-script.gradle' : 'current-init-script.gradle'
      gradle_init_script_opts = "--init-script /opt/gitlab/.gradle/#{gradle_init_script_version}"
      _stdout, _stderr, status = within_project_path do
        shell.execute([
          @command,
          gradle_cli_opts,
          gradle_init_script_opts,
          'downloadLicenses'
        ], env: default_env)
      end

      status.success?
    end

    def wrapper?
      project_path.join('gradlew').exist?
    end

    def xml_parsing_options
      @xml_parsing_options ||= { 'GroupTags' => { 'dependencies' => 'dependency' } }
    end

    def parse_from(xml_file)
      XmlSimple
        .xml_in(xml_file, xml_parsing_options)
        .fetch('dependency', []).map { |hash| map_from(hash) }
    end

    def map_from(hash)
      Dependency.from(GradlePackage.new(hash, include_groups: @include_groups), detected_package_path)
    end

    def default_env
      @default_env = {
        'ASDF_JAVA_VERSION' => ENV.fetch('ASDF_JAVA_VERSION', java_version),
        'CACHE_DIR' => '/opt/gitlab',
        'JAVA_HOME' => ENV.fetch("JAVA_HOME", "/opt/asdf/installs/java/#{java_version}"),
        'TERM' => 'noop'
      }
    end
  end
end
