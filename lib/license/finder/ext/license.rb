# frozen_string_literal: true

module LicenseFinder
  class License
    attr_reader :short_name, :pretty_name, :other_names
  end
end
