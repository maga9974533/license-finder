# frozen_string_literal: true

module LicenseFinder
  class Nuget
    def prepare
      create_vendor_path

      within_project_path do
        tool_box.install(tool: :mono)
        shell.execute([:cert_sync, shell.default_certificate_path], capture: false)
        shell.execute([
          :mono,
          :nuget,
          :restore, detected_package_path,
          '-LockedMode',
          '-NoCache',
          '-PackagesDirectory', vendor_path,
          '-Verbosity', :normal
        ], capture: false)
      end
    end

    def installed?(*)
      true
    end

    def current_packages
      dependencies.map do |dependency|
        nupkg = vendor_path.glob("**/#{dependency.name}*.nupkg")[0]
        ::LicenseFinder::Dependency.new(
          'NuGet',
          dependency.name,
          dependency.version,
          spec_licenses: license_urls(dependency, nupkg),
          detection_path: detected_package_path,
          install_path: nupkg&.parent
        )
      end
    end

    def license_urls(dependency, nupkg)
      return if nupkg.nil? || !nupkg.exist?

      Zip::File.open(nupkg.to_s) do |zipfile|
        content = zipfile.read("#{dependency.name}.nuspec")
        ::License::Management::Nuspec.new(content).licenses
      end
    rescue StandardError => e
      log.error(e)
      []
    end
  end
end
