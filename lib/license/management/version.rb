# frozen_string_literal: true

module License
  module Management
    VERSION = '4.2.0'
  end
end
