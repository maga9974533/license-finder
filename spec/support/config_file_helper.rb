# frozen_string_literal: true

module ConfigFileHelper
  def config_file_content(path)
    full_path = License::Management.root.join("config/files", path)
    IO.read(full_path)
  end
end
