# frozen_string_literal: true
require 'support/proxy_server'

module ProxyHelper
  def x509_certificate
    License::Management.root.join("tmp/wildcard.test.crt")
  end
end

RSpec.configure do |config|
  config.include(ProxyHelper, type: :integration)
  config.before(:suite) do
    ProxyServer.instance.start
  end

  config.after(:suite) do
    ProxyServer.instance.stop
  end
end
