# frozen_string_literal: true

class Report
  attr_reader :report

  def initialize(raw)
    @report = JSON.parse(raw, symbolize_names: true)
  end

  def [](key)
    report[key]
  end

  def dependency_names
    report[:dependencies].map { |x| x[:name] }
  end

  def licenses_for(name)
    (find(name) || {}).fetch(:licenses, [])
  end

  def find(name)
    report[:dependencies].find do |dependency|
      dependency[:name] == name
    end
  end

  def nil?
    report.nil?
  end

  def to_hash
    to_h
  end

  def to_h
    report
  end
end
